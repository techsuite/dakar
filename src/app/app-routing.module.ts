import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home/home.page.component';
import { LoginPageComponent } from './pages/login/login.page.component';
import { ImpostorPageComponent } from './pages/impostor/impostor.page.component';
import { RegistroPageComponent } from './pages/registro/registro.page.component';
import { Registro2PageComponent } from './pages/registro2/registro2.page.component';
import { Registro3PageComponent } from './pages/registro3/registro3.page.component';
import { Registro4PageComponent } from './pages/registro4/registro4.page.component';
import { PaginaBienvenidaComponent } from './pages/pagina-bienvenida/pagina-bienvenida.component';
import { ConsultasComponent } from './pages/consultas/consultas.component';
import { AnalisisComponent } from './pages/analisis/analisis.component';
import { WikiComponent } from './pages/wiki/wiki.component';
import { ComparacionComponent } from './pages/wiki/comparacion/comparacion.component';
import { MedicamentoComponent } from './pages/wiki/medicamento/medicamento.component';
import { AuthGuard } from './guards/auth.guard';
import { BaseComponent } from './pages/base/base.component';
import { ConsultasInfoComponent } from './pages/consultas/consultas-info/consultas-info.component';
import { AnalisisInfoComponent } from './pages/analisis/analisis-info/analisis-info.component';
import { NotFoundComponent } from './pages/notfound/notfound.component';



const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent} ,
  { path: 'registro', component: RegistroPageComponent} ,
  { path: 'registro2', component: Registro2PageComponent} ,
  { path: 'registro3', component: Registro3PageComponent} ,
  { path: 'registro4', component: Registro4PageComponent} ,
  { path: 'base', 
    component:BaseComponent,
    canActivate:[AuthGuard],
    children: [
      { path: 'pagina-bienvenida', component: PaginaBienvenidaComponent},
      { path: 'consultas', component: ConsultasComponent },
      { path: 'analisis', component: AnalisisComponent},
      { path: 'wiki', component: WikiComponent },
      { path: 'wiki/medicamento', component: MedicamentoComponent },
      { path: 'wiki/comparacion', component: ComparacionComponent },  
      { path: 'impostor', component: ImpostorPageComponent} ,
      { path: 'consultas/consultasInfo', component: ConsultasInfoComponent },
      { path: 'analisis/analisisInfo', component: AnalisisInfoComponent }
  ],
  },
  {path: '404', component: NotFoundComponent},
  {path: '**', redirectTo: '/404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
