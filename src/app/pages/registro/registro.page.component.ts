import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from "src/app/model/datosuser/User";
import { UserResponse } from 'src/app/model/datosuser/UserResponse';
import { LoginService } from 'src/app/service/login.service'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.component.html',
  styleUrls: ['./registro.page.component.scss'],
})
export class RegistroPageComponent {
  dni: string;
  mensaje: string;
  email: string;


  constructor(private loginService: LoginService, private router: Router){ }

    
  //Función de registro
   register(): void{
    this.loginService.getCensoredEmail(this.dni).subscribe((email) => {
      
      if (email === null) {
        this.mensaje = 'Datos incorrectos';
      }else{
        this.loginService.setDni(this.dni);
        this.loginService.setEmail(email.correo_electronico);
        this.router.navigate(['/registro2']);
        this.mensaje = '';

      } 
    });

    

  }
}