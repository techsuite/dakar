import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Enfermedad } from 'src/app/model/info-paciente/enfermedad';
import { Medicamento } from 'src/app/model/info-paciente/medicamento';
import { ProductoNatural } from 'src/app/model/info-paciente/productoNatural';
import { MedicamentoService } from 'src/app/service/medicamento.service';

@Component({
  selector: 'app-medicamento',
  templateUrl: './medicamento.component.html',
  styleUrls: ['./medicamento.component.scss'],
})
export class MedicamentoComponent implements OnInit {

  [x: string]: any;

  nombre: string = this.medicamentoService.getNombre();
  tipo:  string = this.medicamentoService.getTipo();; //Medicamento//Enfermedad //Homeopatia
  pagWeb: string = 'google.es';
  datosCargados: boolean = false;

  infoMedicamento: Medicamento = this.medicamentoService.getMedicamento();
  infoEnfermedad: Enfermedad = this.medicamentoService.getEnfermedad();
  infoProductoNatural: ProductoNatural = this.medicamentoService.getproductoNatural();


  infoContraindicaciones: string =
    "Prueba Contraindicaciones -> here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.";
  infoPosologia: string =
    "Prueba Posologia -> here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.";
  infoProspecto: string =
    "Prueba Prospecto -> here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.";
  infoComposicion: string =
    "Prueba Composicion -> here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.";
  infoSintomas: string =
    "Prueba Sintomas -> here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.";
  infoRecomendaciones: string =
    "Prueba Recomendaciones -> here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.";

  constructor(private medicamentoService: MedicamentoService, private router: Router) {}
  
  ngOnInit(): void {

    this.medicamentoService.getInfoMedicamento(this.nombre, 20, 1)
      .subscribe((infoMedicamento) => {
        this.medicamentoService.setMedicamento(infoMedicamento);
        sessionStorage.setItem('busquedainfoMedicamento', JSON.stringify(infoMedicamento)); //guardo en memoria
        this.medicamentoService.getInfoEnfermedad(this.nombre, 20, 1)
          .subscribe((infoEnfermedad) => {
            this.medicamentoService.setEnfermedad(infoEnfermedad);
            sessionStorage.setItem('busquedainfoEnfermedad', JSON.stringify(infoEnfermedad)); //guardo en memoria
            this.medicamentoService.getInfoProductoNatural(this.nombre, 20, 1)
              .subscribe((infoProdNat) => {
                this.medicamentoService.setProducto(infoProdNat);
                sessionStorage.setItem('busquedainfoProducto', JSON.stringify(infoProdNat)); //guardo en memoria
                this.datosCargados = true;

              });

          });

      });


  

  }

  medicamentoDisponible() {
    if (this.medicamentoService.getMedicamento() !== null) {
      this.infoMedicamento = this.medicamentoService.getMedicamento();
    }
    return this.infoMedicamento !== null && this.infoMedicamento !== undefined;
  }
  enfermedadDisponible() {
    if (this.medicamentoService.getEnfermedad() !== null) {
      this.infoEnfermedad = this.medicamentoService.getEnfermedad();
    }
    return this.infoEnfermedad !== null && this.infoEnfermedad !== undefined;
  }
  productoNaturalDisponible() {
    if (this.medicamentoService. getproductoNatural() !== null) {
      this.infoProductoNatural = this.medicamentoService.getproductoNatural();
    }
    return this.infoProductoNatural !== null && this.infoProductoNatural !== undefined;
  }
  volver() {
                            
    this.router.navigate(['/base/wiki']);
  }
}

