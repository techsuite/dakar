import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MedicamentoService } from 'src/app/service/medicamento.service';
import { Medicamento } from 'src/app/model/info-paciente/medicamento';
import { ProductoNatural } from 'src/app/model/info-paciente/productoNatural';


// src/app/model/info-medicamento/medicamento
@Component({
  selector: 'app-comparacion',
  templateUrl: './comparacion.component.html',
  styleUrls: ['./comparacion.component.scss']
})
export class ComparacionComponent implements OnInit {

  //Obtengo los nombres de los medicamentos a comparar
  nombres: Array<string> = this.medicamentoService.getNombres();

  datosCargados: Array<boolean> = new Array();

  infoMedicamento1: Medicamento = this.medicamentoService.getinfoMedicamento1();
  infoProducto1: ProductoNatural = this.medicamentoService.getinfoProducto1();

  infoMedicamento2: Medicamento = this.medicamentoService.getinfoMedicamento2();
  infoProducto2: ProductoNatural = this.medicamentoService.getinfoProducto2();

  datosCargados1: boolean = false;
  datosCargados2: boolean = false;


  infoContraindicaciones: Array<string>=new Array();
  nombresReales: Array<string>=new Array();

  // En la comparación tenemos solo que hacerlo con las contraindicaciones
  infoContraindicacionesAux: string =
    "Prueba Contraindicaciones -> here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.";
  
  
    constructor(private medicamentoService: MedicamentoService,  private router: Router) { }

    accederPagMedicamento(item: string) {
      this.medicamentoService.setNombre(item);
      this.medicamentoService.setEnfermedad(null);
      this.medicamentoService.setMedicamento(null);
      this.medicamentoService.setProducto(null);
  
      this.medicamentoService.guardarNombresMemoria(); //lo guardo tambien en memoria
      this.router.navigate(['base/wiki/medicamento']);
    }
  

  ngOnInit(): void {

     //med1
     this.medicamentoService.getInfoMedicamento(this.nombres[0], 20, 1)
     .subscribe((infoMedicamento) => {
       this.medicamentoService.setinfoMedicamento1(infoMedicamento);
       sessionStorage.setItem('infoMedicamento1', JSON.stringify(infoMedicamento)); //guardo en memoria

       if (this.medicamentoService.getinfoMedicamento1() == null || this.medicamentoService.getinfoMedicamento1() == undefined) {
         this.medicamentoService.getInfoProductoNatural(this.nombres[0], 20, 1)
           .subscribe((infoProdNat) => {
             this.medicamentoService.setinfoProducto1(infoProdNat);
             sessionStorage.setItem('infoProducto1', JSON.stringify(infoProdNat)); //guardo en memoria
           });
       }
       this.datosCargados1 = true;
     });


         //med2
    this.medicamentoService.getInfoMedicamento(this.nombres[1], 20, 1)
    .subscribe((infoMedicamento) => {
      this.medicamentoService.setinfoMedicamento2(infoMedicamento);
      sessionStorage.setItem('infoMedicamento2', JSON.stringify(infoMedicamento)); //guardo en memoria
      

      if (this.medicamentoService.getinfoMedicamento2() == null || this.medicamentoService.getinfoMedicamento2() == undefined) {
        this.medicamentoService.getInfoProductoNatural(this.nombres[1], 20, 1)
          .subscribe((infoProdNat) => {
            this.medicamentoService.setinfoProducto2(infoProdNat);
            sessionStorage.setItem('infoProducto2', JSON.stringify(infoProdNat)); //guardo en memoria
          });
      }
      this.datosCargados2 = true;
    });

}

medEncontrado1(){
  return (this.medicamentoService.getinfoMedicamento1() != null && this.medicamentoService.getinfoMedicamento1() != undefined) 
  ||(this.medicamentoService.getinfoProducto1() != null && this.medicamentoService.getinfoProducto1() != undefined)
}
medEncontrado2(){
  return (this.medicamentoService.getinfoMedicamento2() != null && this.medicamentoService.getinfoMedicamento2() != undefined) 
  ||(this.medicamentoService.getinfoProducto2() != null && this.medicamentoService.getinfoProducto2() !=undefined)
}

medicamentoDisponible1() {
  if (this.medicamentoService.getinfoMedicamento1() !== null) {
    this.infoContraindicaciones[0] = this.medicamentoService.getinfoMedicamento1().data[0].contraindicaciones;
    this.nombresReales[0] = this.medicamentoService.getinfoMedicamento1().data[0].nombre;
  }else if(this.medicamentoService.getinfoProducto1() !== null){
    this.infoContraindicaciones[0] = this.medicamentoService.getinfoProducto1().data[0].contraindicaciones;
    this.nombresReales[0] = this.medicamentoService.getinfoProducto1().data[0].nombre;
  }
  return this.infoContraindicaciones[0] !== null && this.infoContraindicaciones[0] !== undefined;
}

medicamentoDisponible2() {
  if (this.medicamentoService.getinfoMedicamento2() !== null) {
    this.infoContraindicaciones[1] = this.medicamentoService.getinfoMedicamento2().data[0].contraindicaciones;
    this.nombresReales[1] = this.medicamentoService.getinfoMedicamento2().data[0].nombre;
  }else if(this.medicamentoService.getinfoProducto2() !== null){
    this.infoContraindicaciones[1] = this.medicamentoService.getinfoProducto2().data[0].contraindicaciones;
    this.nombresReales[1] = this.medicamentoService.getinfoProducto2().data[0].nombre;
  }
  return this.infoContraindicaciones[1] !== null && this.infoContraindicaciones[1] !== undefined;
}

volver() {
 
  this.router.navigate(['/base/wiki']);
}


}
