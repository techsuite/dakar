import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { MedicamentoService } from 'src/app/service/medicamento.service';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-wiki',
  templateUrl: './wiki.component.html',
  styleUrls: ['./wiki.component.scss']
})

export class WikiComponent implements OnInit {

  formulario: FormGroup;
  numEltos: number = 0;
  field = "Medicamento"; // En un principio buscará por medicamento
  mostrar_mensaje: number = 0;

  constructor(private fb: FormBuilder, private router: Router, private medicamentoService: MedicamentoService, public loginService: LoginService) {}

  //funcion para prevenir fallo en caso de recarga  dudaaaaaa
  generoDisponible() {
    return (this.loginService.getAtribs() !== null);
  }


  crearFormulario() {
    this.formulario = this.fb.group({
      medicamentos: this.fb.array([]),
    });
  }

  get medicamentos(): FormArray {
    return this.formulario.get('medicamentos') as FormArray;
  }

  compararMedicamentos(){
    if(this.field==='Medicamento'){
      this.anadirMedicamento();
    }else{
      this.mostrar_mensaje=4;
    }
  }

  anadirMedicamento() {
      if (this.numEltos < 2) {
        const nuevaFila = this.fb.group({
          text: new FormControl(''),
          botonBorrar: new FormControl(''),
        });
        this.medicamentos.push(nuevaFila);

        this.numEltos++;
        this.mostrar_mensaje = 0; //para asi poder eliminar el mensaje anterior

      }
      else {
        this.mostrar_mensaje = 1;
      }
    }
  

  borrarMedicamento(indice: number) {
    this.medicamentos.removeAt(indice);
    this.numEltos--;
    this.mostrar_mensaje = 0;
  }
  
  guardarTipoMedicamento(field : string){
    this.field=field;
  }

  avanzarPagMedicamento() {
    if (this.medicamentos.value[0].text === ''  || this.medicamentos.value[0].text === null) { //Campos vacios
      this.mostrar_mensaje = 2;
    } else {
      this.medicamentoService.setNombre(this.medicamentos.value[0].text); //guardo el nombre

      this.medicamentoService.setEnfermedad(null);
      this.medicamentoService.setMedicamento(null);
      this.medicamentoService.setProducto(null);

      this.medicamentoService.guardarNombresMemoria(); //lo guardo tambien en memoria
      this.router.navigate(['base/wiki/medicamento']);
    }
  }

  avanzarPagComparacion() {
    let sinRellenar = false;
    let valores = new Array<string>();

    //Bucle de comparacion de medicamentos
    for (
      let index = 0;
      index < this.medicamentos.length && !sinRellenar;
      index++
    ) {
      let valor = this.medicamentos.value[index].text;
      if (valor === '') {
        sinRellenar = true;
        this.mostrar_mensaje = 3
      } else {
        valores.push(valor);
      }
    }
    if (!sinRellenar) {
      this.medicamentoService.setNombres(valores); //guardo los nuevos

      this.medicamentoService.setinfoMedicamento1(null);
      this.medicamentoService.setinfoMedicamento2(null);
      this.medicamentoService.setinfoProducto1(null);
      this.medicamentoService.setinfoProducto2(null);

      this.medicamentoService.guardarNombresMemoria(); //los guardo en memoria
      this.router.navigate(['base/wiki/comparacion']);
    }
  }


  ngOnInit(): void {
    this.crearFormulario();
    this.anadirMedicamento();
    
  }
}
