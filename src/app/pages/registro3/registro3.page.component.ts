import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from "src/app/model/datosuser/User";
import { UserResponse } from 'src/app/model/datosuser/UserResponse';
import { LoginService } from 'src/app/service/login.service'

@Component({
  selector: 'app-registro3',
  templateUrl: './registro3.page.component.html',
  styleUrls: ['./registro3.page.component.scss'],
})
export class Registro3PageComponent {
  dni: string;
  mensaje: string;
  otp: number;

  constructor(private loginService: LoginService, private router: Router){ }   

  ngOnInit(): void {
    //Si he dejado el registro a medias vuelvo al principio
    if (this.loginService.getEmail() === null) {
      this.router.navigate(['registro']);
    }
  }
 

  //Función de registro
   register(): void{
    this.dni = this.loginService.getDni();
    
    this.loginService.getAuthToken(this.dni,this.otp).subscribe(
      (response) => {                           //Next callback
        console.log('response received');
        this.loginService.setAuthId(response.authToken);
        this.router.navigate(['/registro4']);
        this.mensaje = '';
      },
      (error) => {                              //Error callback
        console.log('error');
        this.mensaje = "Datos incorrectos";

      }
    );

    

  }
}