import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConsultarService } from 'src/app/service/consultar.service';
import { LoginService } from 'src/app/service/login.service';
import { PruebasUserService } from 'src/app/service/pruebas-user.service';

@Component({
  selector: 'app-pagina-bienvenida',
  templateUrl: './pagina-bienvenida.component.html',
  styleUrls: ['./pagina-bienvenida.component.scss']
})
export class PaginaBienvenidaComponent {

  nombre: string;
  genero: string;
  constructor(public loginService: LoginService, private router: Router, private consultarService: ConsultarService, private pruebasUserService: PruebasUserService) { }

  // Funcion que obtiene el nombre del usuario
  obtenerNombre() {

    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.nombre = this.loginService.getAtribs().nombre;
    }

    //Devuelvo dicho nombre
    return this.nombre !== null;
  }

  //Funcion que indica si es hombre o mujer
  obtenerGenero() {
    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.genero = this.loginService.getAtribs().genero;
    }

    //Devuelvo dicho nombre
    return this.genero !== null;
  }

  // Para avanzar a pagina de Consultas
  avanzarConsultas() {
    //cargo la info de pacientes y avanzo a la pag
    this.consultarService.getConsultas(this.loginService.getAtribs().dni, this.loginService.getAuthId(), 0, "0", this.consultarService.getNumConsultas(), 1)
      .subscribe((consultas) => {
        this.consultarService.setPacs(consultas);
        sessionStorage.setItem('consultasinfo', JSON.stringify(consultas)); //guardo en memoria
        this.router.navigate(['base/consultas']);
      });
  }

  // Para avanzar a pagina de Analisis
  avanzarAnalisis() {
    //cargo la info de pacientes y avanzo a la pag
    this.pruebasUserService.getPruebas(this.loginService.getAtribs().dni, this.loginService.getAuthId(), 0, "0", "0", this.pruebasUserService.getNumPruebas(), 1)
      .subscribe((pruebas) => {
        this.pruebasUserService.setPacs(pruebas);
        sessionStorage.setItem('pruebasinfo', JSON.stringify(pruebas)); //guardo en memoria
        this.router.navigate(['base/analisis']);
      });
  }


}
