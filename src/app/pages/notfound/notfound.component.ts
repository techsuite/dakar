import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';


@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(private router: Router, private loginService: LoginService){ }

  ngOnInit(): void {
  }

  goToHome(): void {
    this.router.navigate(['/login']);
  }
}
