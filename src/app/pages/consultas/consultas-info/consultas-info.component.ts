import { ANALYZE_FOR_ENTRY_COMPONENTS, Component, OnInit } from '@angular/core';
import { ConsultarService } from 'src/app/service/consultar.service';
import { LoginService } from 'src/app/service/login.service';
import { InfoConsultas } from 'src/app/model/info-paciente/InfoConsultas';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consultas-info',
  templateUrl: './consultas-info.component.html',
  styleUrls: ['./consultas-info.component.scss']
})
export class ConsultasInfoComponent {

  nombre: string;
  apellidos: string;
  dniPaciente: string;
  fecha: string;
  doctorNombre: string;
  observaciones: string;
  consulta: InfoConsultas;

  constructor(public loginService: LoginService, private consultarService: ConsultarService, private router: Router) { }

  ngOnInit() {
    this.consulta = this.consultarService.getInfoConsulta();
    this.consulta = JSON.parse(sessionStorage.getItem('consultasinfoAdic'));
    this.dniPaciente = this.consulta.pacienteDNI;
    this.fecha = this.convertirFecha(this.consulta.fechaRealizacion);
    this.doctorNombre= this.consulta.doctorNombre;
    this.observaciones=this.consulta.observaciones;
  }

  // Para pasar los milisegundos a una fecha del tipo DD/MM/YYYY
  convertirFecha(milsec : number) : string {
    return new Date(milsec).toLocaleDateString();
  }

  obtenerNombre() {
    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.nombre = this.loginService.getAtribs().nombre;
    }
    return this.nombre;
  }
  obtenerApellidos() {
    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.apellidos = this.loginService.getAtribs().apellidos;
    }

    return this.apellidos;
  }

  goToConsultas(){
    this.router.navigate(['/base/consultas']);
  }

}
