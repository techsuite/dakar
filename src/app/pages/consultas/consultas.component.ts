import { Component, Input, OnInit } from '@angular/core';
import { Consultas } from 'src/app/model/info-paciente/Consultas';
import { InfoConsultas } from 'src/app/model/info-paciente/InfoConsultas';
import { ConsultarService } from 'src/app/service/consultar.service';
import { LoginService } from 'src/app/service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consultas',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.scss']
})

export class ConsultasComponent implements OnInit {


  nombre: string;
  apellidos: string;
  consultas: Consultas = this.consultarService.getPacs();
  filterEntry: any;
  field = 'all'; // En un principio buscará por cualquier valor
  infoConsultas: InfoConsultas;

  constructor(public loginService: LoginService, private consultarService: ConsultarService, private router: Router) { }


  obtenerNombre() {
    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.nombre = this.loginService.getAtribs().nombre;
    }
    return this.nombre;
  }
  obtenerApellidos() {
    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.apellidos = this.loginService.getAtribs().apellidos;
    }

    return this.apellidos;
  }

  necesitoCalendario() {
    if (this.field === 'fecha')
      return true;
    return false;
  }

  ngOnInit() : void{
    this.consultarService.setnumPag(1);
  }

  // Para pasar los milisegundos a una fecha del tipo DD/MM/YYYY
  convertirFecha(milsec: number): string {
    return new Date(milsec).toLocaleDateString();
  }

  // Para el filtro:
  onSubmit() {
    this.recargarPag();
    this.filterEntry = null;
  }

  //Para que no salga el objeto
  borrarCampo() {
    this.filterEntry = null;
  }

  restablecer(){
    
    this.field='all';
    this.onSubmit();
    this.borrarCampo();
    
  }

  //Para el boton de mas info
  conseguirInfo(idConsulta: number) {
    
    this.consultarService.getConsultaDetalles(idConsulta, this.loginService.getAuthId())
      .subscribe(
        (infoConsultas) => { // Cuando me contestan
          this.infoConsultas = infoConsultas; // guardo mi objeto consultas
          sessionStorage.setItem('consultasinfoAdic', JSON.stringify(infoConsultas)); //guardo en memoria
          this.consultarService.setInfoConsulta(infoConsultas);
          this.router.navigate(["/base/consultas/consultasInfo"]);
        },
        (error) => {                              //Error callback
          console.log('error');
          alert("Datos incorrectos");

        });
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // Para la paginacion
  sigPag(): void {
    if (this.masPaginasDcha()) {
      this.consultarService.setnumPag(this.consultarService.getnumPag() + 1);
      this.recargarPag();
    }
  }

  antPag(): void {
    if (this.masPaginasIzq()) {
      this.consultarService.setnumPag(this.consultarService.getnumPag() - 1);
      this.recargarPag();
    }
  }

  masPaginasDcha(): boolean {
    return this.consultas?.metadata?.pageNumber !== this.consultas?.metadata?.totalPages;
  }

  masPaginasIzq(): boolean {
    return this.consultas?.metadata?.pageNumber != 1;
  }

  recargarPag() {
    let valorNombre = "0";
    let valorFecha = 0;

    if(this.field == 'doctor'){
      valorNombre = this.filterEntry;
    }else if ( this.field == 'fecha' ){

      const formatDateDigit = digit => digit.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })
      valorFecha = new Date(`${this.filterEntry?.year}-${formatDateDigit(this.filterEntry?.month)}-${formatDateDigit(this.filterEntry?.day)}`).getTime();

      console.log(valorFecha);
    }

      this.consultarService
        .getConsultas(this.loginService.getAtribs().dni, this.loginService.getAuthId(), valorFecha, valorNombre, this.consultarService.getNumConsultas(), this.consultarService.getnumPag())
        .subscribe((consultas) => {
          (this.consultas = consultas);
          this.consultarService.setPacs(consultas);
          sessionStorage.setItem('consultasinfo', JSON.stringify(consultas)); //guardo en memoria
        });
    
  }

}
