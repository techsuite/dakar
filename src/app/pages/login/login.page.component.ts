import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from "src/app/model/datosuser/User";
import { UserResponse } from 'src/app/model/datosuser/UserResponse';
import { LoginService } from 'src/app/service/login.service'

@Component({
  templateUrl: './login.page.component.html',
  styleUrls: ['./login.page.component.scss'],
})
export class LoginPageComponent implements OnInit{

    user: User = new User(); //usuario recibido por login pagina
    userR: UserResponse = new UserResponse();  //respuesta de backend
    authid: number;
    mensaje: string;// si va mal sale este mensaje
    roles: Array<String>;

    constructor(private loginService: LoginService, private router: Router){ }

     
    ngOnInit(): void {
      //si ya me encuentro logineado no tiene sentido volver al inicio, para eso que salga de sesion
      if (this.loginService.isLoggedIn()) {
        this.router.navigate(['base/pagina-bienvenida']);
      }
    }
  
  //comprobacion en el logIn

   logIn(): void{

    this.loginService.getId(this.user).subscribe((AuthId) => {
       if (AuthId === null) {
        this.mensaje = 'Datos incorrectos';
        
      }else{
        this.loginService.setAuthId(AuthId.token);
        sessionStorage.setItem("authId", AuthId.token.toString());
  
        //recojo sus atribs y los guardo
         this.loginService.getUserResponse(AuthId.token).subscribe((userR) => {
                this.userR = userR;
                this.loginService.setUserR(this.userR);
                //navego a la siguiente pagina
                this.loginService.getRoles(AuthId.token).subscribe((roles) => {
                  this.roles=roles;
                  this.loginService.setRoles(roles);
                  if(roles.includes("Impostor")){
                    this.loginService.setDni(this.user.dni);
                    this.router.navigate(['base/impostor'], { state: { usuario: this.userR } });
                  }else{
                    this.router.navigate(['base/pagina-bienvenida'], { state: { usuario: this.userR } });
                    this.mensaje = '';
                  }
                })
                
  
          });
      }
    
    });
  }

  registro(): void{
    this.router.navigate(['/registro']);
  }
}
