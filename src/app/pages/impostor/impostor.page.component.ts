import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from "src/app/model/datosuser/User";
import { UserResponse } from 'src/app/model/datosuser/UserResponse';
import { LoginService } from 'src/app/service/login.service'
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-impostor',
  templateUrl: './impostor.page.component.html',
  styleUrls: ['./impostor.page.component.scss'],
})
export class ImpostorPageComponent {
    token: number;
    tokenR: number;
    userR: UserResponse = new UserResponse();  //respuesta de backend
    mensaje: string;// si va mal sale este mensaje
    dni: string;

    constructor(private loginService: LoginService, private router: Router){ }

    @HostListener('window:popstate', ['$event'])
    onPopState(event) {
      if (confirm('¿Quieres ingresar en tu cuenta?')) {
        // Save it!
        console.log('Thing was saved to the database.');
      } else {
        this.loginService.setAuthId(null);
        console.log('Thing was not saved to the database.');
      }
    }
    
  //comprobacion en el logIn
   logIn(): void{
    this.dni = this.loginService.getDni();

    this.loginService.getToken(this.dni,this.token).subscribe((AuthId) => {
      if (AuthId === null) {
        this.mensaje = 'Datos incorrectos';
      }else{
        this.loginService.setAuthId(AuthId.token);
        sessionStorage.setItem("authId", AuthId.token.toString());
  
        //recojo sus atribs y los guardo
          this.loginService.getUserResponse(AuthId.token).subscribe((userR) => {
                this.userR = userR;
                this.loginService.setUserR(this.userR);
                //navego a la siguiente pagina
                    this.router.navigate(['base/pagina-bienvenida'], { state: { usuario: this.userR } });
                    this.mensaje = '';
          });
      }
    });

  }
}