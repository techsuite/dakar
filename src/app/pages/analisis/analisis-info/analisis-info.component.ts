import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/service/login.service';
import { PruebasUserService } from 'src/app/service/pruebas-user.service';
import { Router } from '@angular/router';
import { InfoPruebas } from 'src/app/model/info-paciente/InfoPruebas';

@Component({
  selector: 'app-analisis-info',
  templateUrl: './analisis-info.component.html',
  styleUrls: ['./analisis-info.component.scss']
})
export class AnalisisInfoComponent {

  nombre: string;
  apellidos: string;
  prueba: InfoPruebas;
  pacienteDNI : string;
  fechaRealizacion : string;
  trabLabDNI : string;
  trabLabNombre : string;
  tipoPrueba : string;
  electrolitos : number;
  glucosa : number;
  colesterol : number;
  trigliceridos : number;
  bilirrubina : number;
  fluorescencia : number;

  constructor(public loginService: LoginService, private pruebasUserService: PruebasUserService, private router: Router) { }
  
  obtenerNombre() {
    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.nombre = this.loginService.getAtribs().nombre;
    }
    this.ngOnInit();
    return this.nombre;
  }
  obtenerApellidos() {
    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.apellidos = this.loginService.getAtribs().apellidos;
    }

    return this.apellidos;
  }
  goToAnalisis(){
    this.router.navigate(['/base/analisis']);
  }
  analisisSangre(){
    if(this.tipoPrueba == "AnalisisSangre")
      return true;
    else{
      return false;
    }
  }

  pruebaPCR(){
    if(this.tipoPrueba == "PCR")
      return true;
    else{
      return false;
    }
  }

  ngOnInit() {
    this.prueba = this.pruebasUserService.getInfoPruebas();
    this.prueba = JSON.parse(sessionStorage.getItem('analisisinfoAdic'));
    this.pacienteDNI = this.prueba.pacienteDNI;
    this.fechaRealizacion = this.convertirFecha(this.prueba.fechaRealizacion);
    this.trabLabDNI= this.prueba.trabLabDNI;
    this.trabLabNombre=this.prueba.trabLabNombre;
    this.tipoPrueba=this.prueba.tipoPrueba;
    this.electrolitos=this.prueba.electrolitos;
    this.glucosa=this.prueba.glucosa;
    this.colesterol=this.prueba.colesterol;
    this.trigliceridos=this.prueba.trigliceridos;
    this.bilirrubina=this.prueba.bilirrubina;
    this.fluorescencia=this.prueba.fluorescencia;

  }

  
  // Para pasar los milisegundos a una fecha del tipo DD/MM/YYYY
  convertirFecha(milsec : number) : string {
    return new Date(milsec).toLocaleDateString();
  }
}
