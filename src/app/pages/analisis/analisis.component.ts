import { Component, OnInit } from '@angular/core';
import { Pruebas } from 'src/app/model/info-paciente/Pruebas';
import { LoginService } from 'src/app/service/login.service';
import { PruebasUserService } from 'src/app/service/pruebas-user.service';
import { InfoPruebas } from 'src/app/model/info-paciente/InfoPruebas';
import { Router } from '@angular/router';

@Component({
  selector: 'app-analisis',
  templateUrl: './analisis.component.html',
  styleUrls: ['./analisis.component.scss']
})
export class AnalisisComponent {

  nombre: string;
  pruebas: Pruebas = this.pruebasUserService.getPacs();
  infoPruebas: InfoPruebas;
  filterEntry: any;
  field = 'all'; // En un principio buscará por cualquier valor

  constructor(public loginService: LoginService, private pruebasUserService: PruebasUserService, private router: Router) { }

  obtenerNombreApellidos() {
    // Cuando paso unos atributos...
    if (this.loginService.getAtribs() !== null) {
      this.nombre = this.loginService.getAtribs().nombre + " " + this.loginService.getAtribs().apellidos;
    }

    //Devuelvo dicho nombre
    return this.nombre !== null;
  }

  necesitoCalendario() {
    if (this.field === 'fecha')
      return true;
    return false;
  }

  ngOnInit(): void { 
    this.pruebasUserService.setnumPag(1);
  }
  restablecer(){
    
    this.field='all';
    this.onSubmit();
    this.borrarCampo();
    
  }
  // Para pasar los milisegundos a una fecha del tipo DD/MM/YYYY
  convertirFecha(milsec: number): string {
    return new Date(milsec).toLocaleDateString();
  }

  // Para el filtro:
  onSubmit() {
    this.recargarPag();
    this.filterEntry = null;
  }

  //Para que no salga el objeto
  borrarCampo() {
    this.filterEntry = null;
  }

  //Para el boton de mas info
  conseguirInfo(idPrueba: number) {

    this.pruebasUserService.getPruebaDetalles(idPrueba, this.loginService.getAuthId())
      .subscribe(
        (infoPruebas) => { // Cuando me contestan
          this.infoPruebas = infoPruebas; // guardo mi objeto pruebas
          sessionStorage.setItem('analisisinfoAdic', JSON.stringify(infoPruebas)); //guardo en memoria
          this.pruebasUserService.setInfoPruebas(infoPruebas);
          this.router.navigate(["/base/analisis/analisisInfo"]);
        },
        (error) => {                              //Error callback
          console.log('error');
          alert("Datos incorrectos");

        });
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // Para la paginacion
  sigPag(): void {
    if (this.masPaginasDcha()) {
      this.pruebasUserService.setnumPag(this.pruebasUserService.getnumPag() + 1);
      this.recargarPag();
    }
  }

  antPag(): void {
    if (this.masPaginasIzq()) {
      this.pruebasUserService.setnumPag(this.pruebasUserService.getnumPag() - 1);
      this.recargarPag();
    }
}

masPaginasDcha(): boolean {
  return this.pruebas?.metadata?.pageNumber != this.pruebas?.metadata?.totalPages;
}

masPaginasIzq(): boolean {
  return this.pruebas?.metadata?.pageNumber != 1;
}

    recargarPag() {
      let valorNombre = "0";
      let valorFecha = 0;
      let tipoPrueba = "0";
  
      if (this.field == 'laboratorio') {
        valorNombre = this.filterEntry;
      } else if (this.field == 'fecha') {
        const formatDateDigit = digit => digit.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })
        valorFecha = new Date(`${this.filterEntry?.year}-${formatDateDigit(this.filterEntry?.month)}-${formatDateDigit(this.filterEntry?.day)}`).getTime();
        console.log(valorFecha);
      } else if (this.field == 'tipoPrueba') {
        tipoPrueba = this.filterEntry;
    }

    this.pruebasUserService
      .getPruebas(this.loginService.getAtribs().dni, this.loginService.getAuthId(), valorFecha, tipoPrueba, valorNombre, this.pruebasUserService.getNumPruebas(), this.pruebasUserService.getnumPag())
      .subscribe((pruebas) => {
        (this.pruebas = pruebas);
        this.pruebasUserService.setPacs(pruebas);
        sessionStorage.setItem('consultasinfo', JSON.stringify(pruebas)); //guardo en memoria
      });

  }

}
