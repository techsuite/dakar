import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from "src/app/model/datosuser/User";
import { UserResponse } from 'src/app/model/datosuser/UserResponse';
import { LoginService } from 'src/app/service/login.service'

@Component({
  selector: 'app-registro2',
  templateUrl: './registro2.page.component.html',
  styleUrls: ['./registro2.page.component.scss'],
})
export class Registro2PageComponent {
  dni: string;
  mensaje: string;
  emailCensored: string;
  email: string;


  constructor(private loginService: LoginService, private router: Router){ }   

  ngOnInit(): void {
    //Si he dejado el registro a medias vuelvo al principio
    if (this.loginService.getEmail() === null) {
      this.router.navigate(['registro']);
    }
  }
  obtenerEmail(){ 
    // Comprobar la existencia del email censurado y asignarlo a la variable correspondiente
    if(this.loginService.getEmail() !== null){
      this.emailCensored = this.loginService.getEmail();
    }
    //Devuelvo el email censurado
    return this.emailCensored !== null;
  }

  //Función de registro
   register(): void{
    this.dni = this.loginService.getDni();
    if(this.email == null){
      this.mensaje = "Datos incorrectos";
    }else{
    this.loginService.generateOTP(this.dni,this.email).subscribe(
      (response) => {                           //Next callback
        
        console.log('response received')
        this.router.navigate(['/registro3']);
        this.mensaje = '';
        
      },
      (error) => {                              //Error callback
        console.log('error');
        this.mensaje = "Datos incorrectos";

      }
    );
    }

    

  }
}