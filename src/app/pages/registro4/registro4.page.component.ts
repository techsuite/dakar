import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/datosuser/User';
import { UserResponse } from 'src/app/model/datosuser/UserResponse';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-registro4',
  templateUrl: './registro4.page.component.html',
  styleUrls: ['./registro4.page.component.scss'],
})
export class Registro4PageComponent {
  dni: string;
  mensaje: string;
  authToken: number;
  password: string;

  constructor(private loginService: LoginService, private router: Router) {}

  ngOnInit(): void {
    //Si he dejado el registro a medias vuelvo al principio
    if (this.loginService.getEmail() === null) {
      this.router.navigate(['registro']);
    }
  }

  //Función de registro
  register(): void {
    this.dni = this.loginService.getDni();
    this.authToken = this.loginService.getAuthId();
    this.loginService
      .setPassword(this.dni, this.authToken, this.password)
      .subscribe(
        (response) => {
          //Next callback
          console.log('response received');
          this.loginService.setAuthId(null);
          this.loginService.setUserR(null);
          sessionStorage.clear();
          //Volvemos a la página de inicio
          this.router.navigate(['/login']);
          this.mensaje = '';
        },
        (error) => {
          //Error callback
          console.log('error');
          this.mensaje = 'Datos incorrectos';
        }
      );
  }
}
