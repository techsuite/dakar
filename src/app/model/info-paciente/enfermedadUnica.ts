export class EnfermedadUnica{
    "idEnfermedad" : 1;
    "nombre" : "string";
    "sintomas" : "string";
    "recomendaciones" : "string";
    "url" : "string";
}