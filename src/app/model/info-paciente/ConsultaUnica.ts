export class ConsultaUnica{
    
    "idConsulta" : number;
    "fechaRealizacion" : Date; //Hay que crear objeto Date con el dato y luego utilizar la funcion .toUTCString())
    "doctorDNI" : string;
    "doctorNombre" : string;
 
}