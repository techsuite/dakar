export class InfoConsultas{
    
    "pacienteDNI" : string;
    "fechaRealizacion" : number;
    "observaciones" : string;
    "doctorDNI" : string;
    "doctorNombre" : string;
 
}