export class UserResponse {
    dni: string;
    apellidos: string;
    correo_electronico: string;
    edad: number;
    genero: string;
    nombre: string;
}
