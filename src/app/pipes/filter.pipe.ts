import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any, field: any): any {
    const resultEntry = [];
//cambiar el filtro para buscar por nombre en lugar de DNI, cambiando el atributo. 
    for(const entry of value){
      if(field ==='all' || (field === 'fecha' && entry.fechaRealizacion.getTime() === new Date(arg.year, arg.month - 1, arg.day).getTime())
          ||(field ==='doctor' && entry.doctorNombre.normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLowerCase().indexOf(arg.toLowerCase()) > -1)){
        resultEntry.push(entry);
      };
      /*if((field ==='Medicamento') || (field === 'Producto natural' )
          ||(field ==='Enfermedad')){
        resultEntry.push(entry);
      };*/
    };

    return resultEntry;
  }

}
