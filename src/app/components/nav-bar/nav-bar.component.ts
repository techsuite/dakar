import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConsultarService } from 'src/app/service/consultar.service';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})

export class NavBarComponent {
  constructor(private loginService: LoginService,private translateService: TranslateService, public router: Router, private consultarService: ConsultarService) {
    translateService.use('es');
  }

  toggleSpanish(){
    this.translateService.use('es') 
  }

  toggleEnglish(){
    this.translateService.use('en') 
  }

  isLoggedIn(){
    return this.loginService.isLoggedIn();
  }
  
  rutaActual(){
    
    if(this.router.url === "/" || this.router.url === "/login"){
      return true;
    }
    return false;
  }
  rutaImpostor(){
    
    if(this.router.url === "/base/impostor"){
      return true;
    }
    return false;
  }
  ruta404(){
    
    if(this.router.url === "/404"){
      return true;
    }
    return false;
  }


  logOut(){
      
      this.loginService.setAuthId(null);
      this.loginService.setUserR(null);
      sessionStorage.clear();
      
      //Volvemos a la página de inicio
      this.router.navigate(['']); //  MIRAR SI ESTO FUNCIONA
  
  }


}