import { Component } from '@angular/core';

@Component({
  selector: 'app-center-layout',
  template: `<ng-content></ng-content>`,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 2rem;
        height: 90vh;
        padding: 5rem;
      }
    `,
  ],
})
export class CenterLayoutComponent {}
