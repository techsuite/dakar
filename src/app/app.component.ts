import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthId } from './model/datosuser/AuthId';
import { ConsultarService } from './service/consultar.service';
import { LoginService } from './service/login.service';
import { MedicamentoService } from './service/medicamento.service';
import { PruebasUserService } from './service/pruebas-user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private translateService: TranslateService, private medicamentoService: MedicamentoService, private loginService: LoginService, private consultarService: ConsultarService, private pruebasUserService: PruebasUserService) {
    translateService.use('es');
  }

  ngOnInit(): void {

    if (sessionStorage.getItem('authId') !== null) {

      this.loginService.setAuthId(+sessionStorage.getItem('authId'));//Para que no se pierdan los datos al refrescar la pagina
      this.loginService.getUserResponse(this.loginService.getAuthId()).subscribe((userResp) => {
        this.loginService.setUserR(userResp);//Para que no se pierda el nombre al refrescar
        this.loadUserData();
      });
    }

    //si recargo asi puedo recoger info medicamento --> AÑADIR LLAMADA A PEKIN PARA RECUPERAR DATOS
    if (sessionStorage.getItem('nombreMedicamento') !== null) {
      this.medicamentoService.setNombre(JSON.parse(sessionStorage.getItem('nombreMedicamento')));
    }
          //caso wiki: Medicamento:  dudaaaaa

    if (sessionStorage.getItem('nombreWiki') !== null) {
      this.medicamentoService.setNombre(sessionStorage.getItem('nombreWiki'));
    }
    if (sessionStorage.getItem('busquedainfoMedicamento') !== null) {
      this.medicamentoService.setMedicamento(JSON.parse(sessionStorage.getItem('busquedainfoMedicamento')));
    }    
    if (sessionStorage.getItem('busquedainfoEnfermedad') !== null) {
      this.medicamentoService.setEnfermedad(JSON.parse(sessionStorage.getItem('busquedainfoEnfermedad')));
    }
    if (sessionStorage.getItem('busquedainfoProducto') !== null) {
      this.medicamentoService.setProducto(JSON.parse(sessionStorage.getItem('busquedainfoProducto')));
    }

    //Caso consultas
    if (sessionStorage.getItem('consultasinfo') !== null) {
      this.consultarService.setPacs(JSON.parse(sessionStorage.getItem('consultasinfo')));
    }

    //Caso analisis
    if (sessionStorage.getItem('pruebasinfo') !== null) {
      this.pruebasUserService.setPacs(JSON.parse(sessionStorage.getItem('pruebasinfo')));
    }

    if (sessionStorage.getItem('paginaActual') !== null) {
      this.consultarService.setnumPag(parseInt(sessionStorage.getItem('paginaActual')));
    }


  }

  private loadUserData() {
     //Le paso los atributos y en el suscribe pongo lo que devuelve 
    this.consultarService.getConsultas(this.loginService.getAtribs()?.dni, this.loginService.getAuthId(), 0, "0", this.consultarService.getNumConsultas(), 1)
      .subscribe((consultas) => { // Cuando me contestan
      this.consultarService.setPacs(consultas);
      sessionStorage.setItem('consultasinfo', JSON.stringify(consultas)); //guardo en memoria
   });

      //Le paso los atributos y en el suscribe pongo lo que devuelve 
      this.pruebasUserService.getPruebas(this.loginService.getAtribs()?.dni, this.loginService.getAuthId(), 0, "0","0" ,this.pruebasUserService.getNumPruebas(), 1)
      .subscribe((pruebas) => { // Cuando me contestan
      this.pruebasUserService.setPacs(pruebas);
      sessionStorage.setItem('pruebasinfo', JSON.stringify(pruebas)); //guardo en memoria
   });
  }
}
