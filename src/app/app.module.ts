import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LoginPageComponent } from './pages/login/login.page.component';
import { HomePageComponent } from './pages/home/home.page.component';
import { CenterLayoutComponent } from './layouts/center/center.layout.component';
import { NavBarElementComponent } from './components/nav-bar/nav-bar-element/nav-bar-element.component';
import { PaginaBienvenidaComponent } from './pages/pagina-bienvenida/pagina-bienvenida.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ConsultasComponent } from './pages/consultas/consultas.component';
import { AnalisisComponent } from './pages/analisis/analisis.component';
import { WikiComponent } from './pages/wiki/wiki.component';
import { FilterPipe } from './pipes/filter.pipe';
import { NavButtonComponent } from './components/nav-button/nav-button.component';
import { ComparacionComponent } from './pages/wiki/comparacion/comparacion.component';
import { MedicamentoComponent } from './pages/wiki/medicamento/medicamento.component';
import { BaseComponent } from './pages/base/base.component';
import { TitleParrafoComponent } from './components/title-parrafo/title-parrafo.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ImpostorPageComponent } from './pages/impostor/impostor.page.component';
import { ConsultasInfoComponent } from './pages/consultas/consultas-info/consultas-info.component';
import { RegistroPageComponent } from './pages/registro/registro.page.component';
import { Registro2PageComponent } from './pages/registro2/registro2.page.component';
import { Registro3PageComponent } from './pages/registro3/registro3.page.component';
import { Registro4PageComponent } from './pages/registro4/registro4.page.component';
import { AnalisisInfoComponent } from './pages/analisis/analisis-info/analisis-info.component';
import { NotFoundComponent } from './pages/notfound/notfound.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LoginPageComponent,
    ImpostorPageComponent,
    RegistroPageComponent,
    Registro2PageComponent,
    Registro3PageComponent,
    Registro4PageComponent,
    HomePageComponent,
    CenterLayoutComponent,
    NavBarElementComponent,
    PaginaBienvenidaComponent,
    ConsultasComponent,
    AnalisisComponent,
    WikiComponent,
    FilterPipe,
    NavButtonComponent,
    ComparacionComponent,
    MedicamentoComponent,
    BaseComponent,
    TitleParrafoComponent,
    ConsultasInfoComponent,
    AnalisisInfoComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
