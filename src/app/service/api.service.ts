import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ExceptionService } from './exception.service';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  // readonly SERVER = 'https://pekin.juliocastrodev.duckdns.org';
  readonly SERVER = 'http://localhost:8080';

  constructor(
    private http: HttpClient,
    private exceptionService: ExceptionService
  ) {}

  //Para un mapa que haga paginacion
  get<T>(
    endpoint: string,
    queryParams?: { [key: string]: string }
  ): Observable<T> {
    return this.execute('GET', endpoint);
  }

  post<T>(endpoint: string, body: any): Observable<T> {
    return this.execute('POST', endpoint, body);
  }

  // Cuando haya error se va a meter por la rama de errores y va a devolver un null
  private execute<T>(
    method: 'GET' | 'POST',
    endpoint: string,
    body?: any
  ): Observable<T> {
    return this.http
      .request<T>(method, `${this.SERVER}/${endpoint}`, { body })
      .pipe(
        catchError((error: HttpErrorResponse) => {
          this.exceptionService.handleHttpError(error);
          return of(null); // Le llega al usuario un null
        })
      );
  }
}
