import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Enfermedad } from '../model/info-paciente/enfermedad';
import { Medicamento } from '../model/info-paciente/medicamento';
import { ProductoNatural } from '../model/info-paciente/productoNatural';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class MedicamentoService {

   // nombre: string; //caso medicamento unico
   nombres: Array<string> = new Array();
   tipo : string ;// tipo: medicamento, homeopatia o enfermedad

   constructor(private api: ApiService) { }

  medicamento: Medicamento
  medicamentoDetalle: Medicamento

  enfermedad: Enfermedad
  productoNatural: ProductoNatural

  infoMedicamento1: Medicamento
  infoProducto1: ProductoNatural
  infoMedicamento2: Medicamento
  infoProducto2: ProductoNatural

  tamanoMedicamentos: number = 2;
  numPag: number = 1;

// de madrid:
  //caso comparacion

  getinfoProducto1(): ProductoNatural {
    return this.infoProducto1;
  }
  getinfoMedicamento1(): Medicamento {
    return this.infoMedicamento1;
  }
  getinfoProducto2(): ProductoNatural {
    return this.infoProducto2;
  }
  getinfoMedicamento2(): Medicamento {
    return this.infoMedicamento2;
  }

  setinfoProducto1(p: ProductoNatural ){
    this.infoProducto1=p;
  }
  setinfoMedicamento1(m: Medicamento ){
    this.infoMedicamento1=m;
  }
  setinfoProducto2(p: ProductoNatural) {
    this.infoProducto2=p;
  }
  setinfoMedicamento2(m: Medicamento) {
    this.infoMedicamento2=m;
  }

    //caso medicamento 
      //medicamento y medicamentoDetalle
    getMedicamento() {
      return this.medicamento;
    }
  
    setMedicamento(med: Medicamento) {
      this.medicamento = med;
    }
  
    getMedicamentoDetalle() {
      return this.medicamentoDetalle;
    }
  
    setMedicamentoDetalle(med: Medicamento) {
      this.medicamentoDetalle = med;
    }

      //enfermedad y enfermedadDetalle

    getEnfermedad(): Enfermedad {
      return this.enfermedad;
    }
  
    setEnfermedad(enf: Enfermedad) {
      this.enfermedad = enf;
    }
  
      //productoNatural   

    getproductoNatural(): ProductoNatural {
      return this.productoNatural;
    }
  
    setProducto(prod: ProductoNatural) {
      this.productoNatural = prod;
    }

      //paginación

    getTamanoPagMed(): number {
      return this.tamanoMedicamentos;
    }
    getNumPagMed(): number {
      return this.numPag;
    }
  
    setNumPagMed(num: number) {
      this.numPag = num;
      sessionStorage.setItem('paginaActualMedicamento', this.numPag.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
    }
  
    getMedicamentos(dniPaciente: string, tokenUsuario: number, tamanoPag: number, pagina: number): Observable<Medicamento> {
      return this.api.get('getMedicamentos/' + dniPaciente + '/' + tokenUsuario + '/' + tamanoPag + '/' + pagina); //0's para filtrar por dni y fecha (sin hacer)
    }
  
   //esta para recoger la info del elemento

     
    getInfo(nombre: string): Observable<string> { //Array[string]
     return this.api.get('llamada/' + nombre);
   }

   
  // Para recuperar nombre del medicamento
    getNombre(): string {
      return this.nombres[0]; //si hago un pop lo saco por lo que al ir atras y delante no tendria nada
    }

     // Para recuperar los nombres de los medicamentos a comparar
   getNombres(): Array<string> {
     return this.nombres;
   }
  
   getTipo():string{
     return this.tipo;
   }
   setNombre(nombre: string) {
    this.nombres = [nombre];
   }
   setNombres(nombres: Array<string>) {
     this.nombres = nombres;
   }
 
   guardarNombresMemoria() {
     sessionStorage.setItem('nombresWiki', JSON.stringify(this.nombres));
   }

   getInfoMedicamento(nombre: string,  pagina: number, numPag: number): Observable<Medicamento>{
    return this.api.get('getInfoMedicamento?nombre='+nombre+'&pageSize='+pagina+'&pageNumber='+numPag);
   }
   getInfoProductoNatural(nombre: string,  pagina: number, numPag: number): Observable<ProductoNatural>{
    return this.api.get('getInfoProductoNatural?nombre='+nombre+'&pageSize='+pagina+'&pageNumber='+numPag);
   }
   getInfoEnfermedad(nombre: string,  pagina: number, numPag: number): Observable<Enfermedad>{
    return this.api.get('getInfoEnfermedad?nombre='+nombre+'&pageSize='+pagina+'&pageNumber='+numPag);
   }
}
