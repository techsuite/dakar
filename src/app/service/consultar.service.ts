import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Consultas } from '../model/info-paciente/Consultas';
import { InfoConsultas } from '../model/info-paciente/InfoConsultas';
import { ApiService } from './api.service';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class ConsultarService {

  constructor(private api: ApiService, public loginService: LoginService) { }

  consulta : InfoConsultas;

  /*Get en /getConsultas
  Enviar: https://pekin.juliocastrodev.duckdns.org/getConsultas/{dniPaciente}/{tokenUsuario}/{fechaFiltro}/{doctorDNIFiltro}/{tamanoPag}/{pagina}
 *******cambiar lo del dni por nombre
  Recibir: Lista con todas las consultas y metadata(codigo 200), código 204 en caso de error 
  con dniPaciente o tokenUsuario y 400 si se solicita una página que esta fuera del rango. 
  SI el usuario que realiza esta llamada no es un doctor o el paciente al que corresponde 
  la consulta se devuelve código 401(unauthorized). Si no se desea filtrar habrá que enviar 
  en los campos fechaFiltro y doctorDNIFiltro(que sirven para filtrar las consultas que tiene
  el paciente) un 0 y en caso de error con estos campos se devolverá 204. La fecha habrá que 
  enviarla como un entero(ms desde 1970).

*/
// cambio doctorDNIFiltro por doctorNombreFiltro

  getConsultas(dni: string, token: number, fechaFiltro: number,doctorNombreFiltro: string, tamanoPag: number, pagina:number): Observable<Consultas>{
    return this.api.get('getConsultas/'+dni+'/'+token+'/'+ fechaFiltro + '/' + doctorNombreFiltro + '/' + tamanoPag + '/' + pagina);
  }


  /*

  Get en /getConsultaDetalles

Enviar: https://pekin.juliocastrodev.duckdns.org/getConsultaDetalles/{idConsulta}/{tokenUsuario}

Recibir: Los detalles de la consulta(código 200). En caso de que el idConsulta o 
tokenUsuario no exista se devolverá 204. Si tokenUsuario no corresponde a un doctor 
o al paciente al que corresponde la consulta se devolverá 401(unauthorized).

  */

getConsultaDetalles(id: number, token: number): Observable<InfoConsultas>{
  return this.api.get('getConsultaDetalles/'+id+'/'+token);
}


  numPag: number = 1;
  consultas: Consultas;
  numConsultas: number = 4;


  getNumConsultas(): number {
    return this.numConsultas;
  }


  getnumPag() {
    return this.numPag;
  }

  setnumPag(num: number) {
    this.numPag = num;
    sessionStorage.setItem('paginaActual', this.numPag.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
  }

  setInfoConsulta(infoConsulta: InfoConsultas): void {
    this.consulta = infoConsulta;
  }
  getInfoConsulta(): InfoConsultas {
    return this.consulta;
  }

  getPacs() {
    return this.consultas;
  }
  setPacs(pacs: Consultas) {
    this.consultas = pacs;
  }
/*
  //Atributos para guardar los datos y recuperarlos al recargar la pagina.
  fecha:  Date;
  doctor_nombre: string;
  DNI_doctor: string;
  info: string;


  setDniDoctor(dni: string) {
    this.DNI_doctor = dni;
  }
  getDniDoctor(): string {
    return this.DNI_doctor;
  }

  setNombreDoctor(nombre: string) {
    this.doctor_nombre = nombre;
  }
  getNombreDoctor(): string {
    return this.doctor_nombre;
  }
*/
  setConsulta(consultas: Consultas) {
    this.consultas = consultas;
  }
  getConsulta(): Consultas {
    return this.consultas;
  }
}
