import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable({ providedIn: 'root'})
export class ExceptionService{

    constructor(private router: Router){}

    // La logica del error esta aqui
    handleHttpError(error: HttpErrorResponse){
        switch (error.status){
            case 404:
                break;
            case 204:
                console.log("Hay un error con dniPaciente/idConsulta o tokenUsuario");
                break;
            case 400:
                console.log("Solicitud de una pagina fuera de rango");
                break;
            case 401:
                console.log("tokenUsuario no corresponde a un doctor, trabajador de laboratorio o el paciente al que corresponde la consulta");
                this.router.navigate(['/']);
                break;
            case 204:
                console.log("Error en el filtro");
                break;
            

        }
    }
} 