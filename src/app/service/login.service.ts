import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../model/datosuser/User';
import { UserResponse } from '../model/datosuser/UserResponse';
import { AuthId } from '../model/datosuser/AuthId';
import { Email } from '../model/datosuser/Email';
import { UserEmail } from '../model/datosuser/UserEmail';
import { DniOtp } from '../model/datosuser/DniOtp';
import { DniAuthPass } from '../model/datosuser/DniAuthPass';
import { ApiService } from './api.service';
import { getLocaleDayNames } from '@angular/common';

@Injectable({
	providedIn: 'root'
})
export class LoginService {

  private authId: number = null;
  private userR: UserResponse = null;
  private dni: string = null;
  private email: string = null;
  private roles: Array<String> = null;
  private userEmail: UserEmail = null;
  private dniOtp : DniOtp = null;
  private dniAuthPass: DniAuthPass = null;

  constructor(private api: ApiService){}

/*Get en /loginStatus

Enviar: https://pekin.juliocastrodev.duckdns.org/loginStatus/{dni}/{password}

Recibir: Objeto "Entero"(que tiene solo un atributo int id) o código de error 204(no content)
{
  "id" : 0
}*/
  getId(user: User): Observable<AuthId>{
    return this.api.get('loginStatus/'+user.dni+'/'+user.password);
  }

  /*Get en /user

Enviar:  https://pekin.juliocastrodev.duckdns.org/user/{id}

Recibir: UserResponse o código de error 204(no content)
{
    "nombre": "string",
    "apellidos": "string",
    "genero": "string",
    "correo_electronico": "string",
    "edad": 20
}*/
getUserResponse(id: number): Observable<UserResponse> {
   return this.api.get('user/'+id);
    }


/*Get en /getRoles

Enviar: https://pekin.juliocastrodev.duckdns.org/getRoles/{id}

Recibir: Lista<String> o código de error 204(no content)
{
  "string",
  "string",
  "string"
}*/
//getRoles, mando token, recibo lista de string
getRoles(id: number): Observable<Array<string>>{
  return this.api.get('getRoles/' + id);
}


/*
get en  /loginStatus/impostor: 	
Enviar: https://pekin.juliocastrodev.duckdns.org/loginStatus/impostor/{dniImpostor}/{tokenSuplantar}

Enviamos a pekin el dni del impostor y el token de la persona a suplantar 
y recibimos el token del usuario al que va a suplantar(codigo 200) o código de error 204(no content)
{
   "token" : 0
}
*/
getToken(dniImpostor: string, token: number): Observable<AuthId> {
  return this.api.get('loginStatus/impostor/'+dniImpostor+'/'+token);
}

/*
/register/exists : esta llamada envía a pekin DNI y recibimos, en caso de existir un código 200 y el email 	censurado y en caso de error un código 204 
Enviar: https://pekin.juliocastrodev.duckdns.org/register/exists/{DNI}
{ "correo_electronico": "string"}
*/
getCensoredEmail(dni: string): Observable<Email> {
  return this.api.get('register/exists/'+dni);
}

/*
Post en /register/generateOTP
	Enviar: 
{
  "dni": "string",
  "correo_electronico" : "string"
}
	Recibir: código 200 en caso de éxito o 400 en caso de error (el corre electrónico no
	coincide o el usuario ya tiene contraseña)
*/
generateOTP(dni: string, email: string): Observable<any> {
  this.userEmail = new UserEmail();
  this.userEmail.dni = dni;
  this.userEmail.correo_electronico = email;
  return this.api.post('register/generateOTP', this.userEmail);
}

/*
/register/generateAuthToken : esta llamada envía a pekin un DNI y un OTP (sería la clave enviada previamente al correo electrónico) 
{
  "dni": "string",
  "otp": 12345 (será un entero)
}
y recibimos, token de autorización(código 200) en caso de token autorizado, y código 400 en caso de error. 
{
  "authToken": 12345 (será un entero)
}
*/
getAuthToken(dni: string,otp: number): Observable<any> {
  this.dniOtp = new DniOtp();
  this.dniOtp.dni = dni;
  this.dniOtp.otp = otp;
  return this.api.post('/register/generateAuthToken',this.dniOtp);
}

/*
/register/setPassword: esta llamada envía a pekin un dni, el token de autenticación(authToken) y password 
{
  "dni": "string",
  "authToken": 12345 (será un entero),
  "password": "string"
}
y recibimos, codigo 200 en caso de exito y código 400 en caso de error (este correspondería a token caducado.)
*/
setPassword(dni: string,authToken: number, password: string): Observable<any> {
  this.dniAuthPass = new DniAuthPass();
  this.dniAuthPass.dni = dni;
  this.dniAuthPass.authToken = authToken;
  this.dniAuthPass.password = password;
  return this.api.post('/register/setPassword',this.dniAuthPass);
}


setRoles(roles: Array<string>) {
  this.roles = roles;
}

setEmail(email: string): void {
  this.email = email;
 }
 
getEmail(): string {
   return this.email;
}

setDni(dni: string): void {
 this.dni = dni;
}

getDni(): string {
  return this.dni;
}

getAtribs(): UserResponse {
  return this.userR;
}

setUserR(userR: UserResponse) {
  this.userR = userR;
}

isLoggedIn(): boolean { 
  return this.authId !== null; 
}

isClient(): boolean {
  return this.authId === 2;
}

setAuthId(authId: number) {
  this.authId = authId;
}

getAuthId(): number {
  return this.authId;
}

}

