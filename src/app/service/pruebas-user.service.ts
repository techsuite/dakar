import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pruebas } from '../model/info-paciente/Pruebas';
import { ApiService } from './api.service';
import { InfoPruebas } from '../model/info-paciente/InfoPruebas';

@Injectable({
  providedIn: 'root'
})
export class PruebasUserService {

  constructor(private api: ApiService) { }

  prueba : InfoPruebas;
/*
Get en /getPruebas

Enviar: https://pekin.juliocastrodev.duckdns.org/getPruebas/{dniPaciente}/{tokenUsuario}/{fechaFiltro}/{tipoPruebaFiltro}/{trabLabDNIFiltro}/{tamanoPag}/{pagina}

*** cambiar dni por nombre en la line anterior
Recibir: Lista con todas las pruebas y metadata(codigo 200), código 204 en caso de error con el dniPaciente
o tokenUsuario y 400 si se solicita una página que esta fuera del rango.SI el usuario que realiza esta llamada
no es un doctor o el paciente al que corresponde la prueba se devuelve código 401(unauthorized). Si no se desea
filtrar habrá que enviar en los campos fechaFiltro,tipoPruebaFiltro y trabLabDNIFiltro(que sirven para filtrar 
las pruebas que tiene el paciente) un 0 y en caso de error con estos campos se devolverá 204. La fecha habrá 
que enviarla como un entero(ms desde 1970).

*/

//cambio trabLabDNIFiltro por trabLabNombreFiltro

getPruebas(dni: string, token: number, fechaFiltro: number,tipoPruebaFiltro: string, trabLabNombreFiltro: string, tamanoPag: number, pagina:number): Observable<Pruebas>{
  return this.api.get('getPruebas/'+dni+'/'+token+'/'+ fechaFiltro + '/' + tipoPruebaFiltro + '/' + trabLabNombreFiltro +'/'+ tamanoPag + '/' + pagina);
}

/*
Enviar: https://pekin.juliocastrodev.duckdns.org/getPruebaDetalles/{idPrueba}/{tokenUsuario}

Recibir:  Los detalles de la prueba(código 200). En caso de que el idConsulta o tokenUsuario no exista se devolverá 204. 
Si tokenUsuario no corresponde a un doctor  o al paciente al que corresponde la prueba se devolverá 401(unauthorized).
*/
getPruebaDetalles(id: number, token: number): Observable<InfoPruebas>{
  return this.api.get('getPruebaDetalles/'+id+'/'+token);
}

numPag: number = 1;
pruebas: Pruebas;
numPruebas: number = 4;


  getNumPruebas(): number {
    return this.numPruebas;
  }


  getnumPag() {
    return this.numPag;
  }

  setnumPag(num: number) {
    this.numPag = num;
    sessionStorage.setItem('paginaActual', this.numPag.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
  }

  setInfoPruebas(infoPruebas: InfoPruebas): void {
    this.prueba = infoPruebas;
  }
  getInfoPruebas(): InfoPruebas {
    return this.prueba;
  }

  getPacs() {
    return this.pruebas;
  }
  setPacs(pacs: Pruebas) {
    this.pruebas = pacs;
  }
}


